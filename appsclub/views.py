from django.contrib import messages,auth
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render,redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from datetime import datetime,timedelta,date
from django.db import connection
from django.db.models import Q

from sclub.appsclub.models import Diario, Pedidos, Promos, Envolturas, Rellenos, Adicionales
#from misitio.ai.forms import cambios


# from gmiproj.gmiapp.forms import CuidadoresForm # POR CADA MODELO una línea de estas

def login_ini(request):
    #return HttpResponse("!! Sistema momentaneamente en etapa de pruebas y avance de desarrollo!!")
    variable1 = 'Pantalla de Acceso al Sistema'
    error_log = 'ok'
    username = request.POST.get('username')
    password = request.POST.get('password') # valor del template
    user = auth.authenticate(username=username, password=password)
    if request.method == "POST":
        if user is not None and user.is_active:
            #Correct password, and the user is marked "active"
            auth.login(request, user)
            request.session['username_x'] = username # variable gobal
            request.session['id_x'] = user.id   # variable gobal
            return HttpResponseRedirect("principal")
            
        error_log = "error"

    context = {'user':user,"variable1":variable1,"error_log":error_log,}

    return render(request,'login_ini.html',context)
    #return render(request,'principal_flex2.html',context)


def log_out(request):
	logout(request)
	return redirect('login_ini')
	#return HttpResponse("Se ha abandonado la aplicación...")


def principal(request):
    variable1 = 'PAGINA PRINCIPAL'
    logo2 = "/static/img/Logo_sc.jpg"
    #logo = "/staticfiles/img/Logo_AsistenciaIntegral.jpg" # for PythonAnyWhere
    prs =  Promos.objects.all().order_by('piezas')
    horas =['17:00', '18:00', '19:00', '20:00','21:00'] #provisorio, despues debe hacerce desde tabla
    # Agregando HORAS DISPONIBLES al arreglo        
    string_horas =""
    aDisponibles = [] 
    for j in horas:
        aDisponibles.append(j)
        string_horas = string_horas + j+", "
    string_horas = string_horas[:-2]  # quita los 2 últimos caracteres (lado derecho quita la coma y el pespacio)
    string_horas = string_horas + " hrs."  

    context ={
        "string_horas":string_horas,
        "prs":prs,
    	"variable1":variable1,
    	"logo_corp_chico":logo2, }

    npromo = 1
    kontador = 0      
    kontador2 = 0
    if request.method == "POST":
        #inicializa variales
        cod1_x,descrip1_x,valor1_x,incluye1_x='','',0,''
        cod2_x,descrip2_x,valor2_x,incluye2_x='','',0,''
        cod3_x,descrip3_x,valor3_x,incluye3_x='','',0,''
        cod4_x,descrip4_x,valor4_x,incluye4_x='','',0,''
        envolt1,envolt2,envolt3,envolt4,envolt5,envolt6,envolt7 = "","","","","","",""
        envolt1_2,envolt2_2,envolt3_2,envolt4_2,envolt5_2,envolt6_2,envolt7_2 = "","","","","","",""

        caja1 = request.POST.get('caja1')    # valor desde el template el identificatorio es el <name>
        caja2 = request.POST.get('caja2')    # valor desde el template el identificatorio es el <name>
        caja3 = request.POST.get('caja3')    # valor desde el template el identificatorio es el <name>
        caja4 = request.POST.get('caja4')    # valor desde el template el identificatorio es el <name>
        #
        #
        adicionales = Adicionales.objects.all().order_by('cod')
        z=1
        for adicio in adicionales:
            if z==1:
                descrip_adi1 = adicio.descrip
                valor_adi1 = adicio.valor
            if z==2:
                descrip_adi2 = adicio.descrip
                valor_adi2 = adicio.valor
            if z==3:
                descrip_adi3 = adicio.descrip
                valor_adi3 = adicio.valor
            z=z+1    

        # Llenando arreglos para subtotales       
        aSubtotal1_adic = []
        aSubtotal2_adic = []
        aSubtotal3_adic = [] 
        i = 1
        while i < 9:
            aSubtotal1_adic.append(valor_adi1 * i)
            aSubtotal2_adic.append(valor_adi2 * i)
            aSubtotal3_adic.append(valor_adi3 * i)
            i += 1

        # combo del cual seleccionar por que cambiar (todos los tipos de envolturas disponibles)
        para_envol = Envolturas.objects.filter(cod='EN').order_by('roll').only('envolt', 'roll','valor')

        relle1  =  Rellenos.objects.filter(cod=0,roll=0)
        relle2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle3  =  Rellenos.objects.filter(cod=0,roll=0)
        relle4  =  Rellenos.objects.filter(cod=0,roll=0)
        relle5  =  Rellenos.objects.filter(cod=0,roll=0)
        relle6  =  Rellenos.objects.filter(cod=0,roll=0)
        relle7  =  Rellenos.objects.filter(cod=0,roll=0)

        relle1_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle2_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle3_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle4_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle5_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle6_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle7_2  =  Rellenos.objects.filter(cod=0,roll=0)


        # TODOS LOS RELLENOS DISPONIBLES PARA SELECCIONAR CAMBIAR
        relle_tot = Rellenos.objects.filter(cod="RE")
        #PRMERA PROMO SELECCIONADA
        if  caja1 != "":
            pr1 = Promos.objects.filter(Q(descrip__icontains=caja1))
            for sal in pr1:
                cod1_x = sal.cod
                descrip1_x = sal.descrip
                valor1_x = sal.valor
                incluye1_x = sal.incluye

            #ENVOLTURAS    
            envolt1 = Envolturas.objects.filter(cod=cod1_x).order_by('roll')
            kontador = 1
            for itera_envolt in envolt1:
                if kontador == 1:
                   envolt1 = itera_envolt.envolt  
                if kontador == 2: 
                   envolt2 = itera_envolt.envolt 
                if kontador == 3: 
                   envolt3 = itera_envolt.envolt 
                if kontador == 4: 
                   envolt4 = itera_envolt.envolt
                if kontador == 5: 
                   envolt5 = itera_envolt.envolt 
                if kontador == 6: 
                   envolt6 = itera_envolt.envolt 
                if kontador == 7: 
                   envolt7 = itera_envolt.envolt 

                kontador = kontador + 1    

            #return HttpResponse(envolt1+" "+envolt2+" "+envolt3+" "+envolt4+" "+envolt5+" "+envolt6+" "+envolt7)    


        #SEGUNDA PROMO SELECCIONADA SEGUNDA PROMO SELECCIONADA SEGUNDA PROMO SELECCIONADA SEGUNDA PROMO SELECCIONADA 
        if  caja2 != "" or  caja2 == None:
            npromo = npromo + 1
            pr2 = Promos.objects.filter(Q(descrip__icontains=caja2))      
            for sal in pr2:
                cod2_x = sal.cod
                descrip2_x = sal.descrip
                valor2_x = sal.valor
                incluye2_x = sal.incluye
            #ENVOLTURAS    
            qr_envolt2  =  Envolturas.objects.filter(cod=cod2_x).order_by('roll')
            kontador2 = 1
            for itera_envolt in qr_envolt2:
                if kontador2 == 1:
                   envolt1_2 = itera_envolt.envolt

                if kontador2 == 2: 
                   envolt2_2 = itera_envolt.envolt

                if kontador2 == 3: 
                   envolt3_2 = itera_envolt.envolt

                if kontador2 == 4: 
                   envolt4_2 = itera_envolt.envolt

                if kontador2 == 5: 
                   envolt5_2 = itera_envolt.envolt

                if kontador2 == 6: 
                   envolt6_2 = itera_envolt.envolt

                if kontador2 == 7: 
                   envolt7_2 = itera_envolt.envolt

                kontador2 = kontador2 + 1    

        if caja3 != "":
            pr3 = Promos.objects.filter(Q(descrip__icontains=caja3))         
            nRoll = 1   
            for sal in pr3:
                cod3_x = sal.cod
                descrip3_x = sal.descrip
                valor3_x = sal.valor
                incluye3_x = sal.incluye

            envolt3  =  Envolturas.objects.filter(cod=cod3_x)
            kontador = 1
            for envolt in envolt3:
                if kontador == 1:
                   envolt1_3 = envolt.envolt 

                if kontador == 2: 
                   envolt2_3 = envolt.envolt 

                if kontador == 3: 
                   envolt3_3 = envolt.envolt 

                if kontador == 4: 
                   envolt4_3 = envolt.envolt 

                if kontador == 5: 
                   envolt5_3 = envolt.envolt 

                if kontador == 6: 
                   envolt6_3 = envolt.envolt 

                if kontador == 7: 
                   envolt7_3 = envolt.envolt

                kontador = kontador + 1    

        if caja4 != "":
            pr4 = Promos.objects.filter(Q(descrip__icontains=caja4))         
            nRoll = 1   
            for sal in pr4:
                cod4_x = sal.cod
                descrip4_x = sal.descrip
                valor4_x = sal.valor
                incluye4_x = sal.incluye
            envolt4  =  Envolturas.objects.filter(cod=cod4_x)
            kontador = 1
            for envolt in envolt4:
                if kontador == 1:
                   envolt1 = envolt.envolt  

                if kontador == 2: 
                   envolt2 = envolt.envolt 

                if kontador == 3: 
                   envolt3 = envolt.envolt 

                if kontador == 4: 
                   envolt4 = envolt.envolt 

                if kontador == 5: 
                   envolt5 = envolt.envolt 

                if kontador == 6: 
                   envolt6 = envolt.envolt 

                if kontador == 7: 
                   envolt7 = envolt.envolt 

                kontador = kontador + 1    


        #para mostrar rellenos disponibles - promo1 - columna 1
        kontador = kontador - 1
        if kontador == 2:    
             relle1  =  Rellenos.objects.filter(cod=20,roll=1)
             relle2  =  Rellenos.objects.filter(cod=20,roll=2)
        
        if kontador == 3:    
             relle1  =  Rellenos.objects.filter(cod=30,roll=1)
             relle2  =  Rellenos.objects.filter(cod=30,roll=2)
             relle3  =  Rellenos.objects.filter(cod=30,roll=3)
        
        if kontador == 4:    
             relle1  =  Rellenos.objects.filter(cod=40,roll=1)
             relle2  =  Rellenos.objects.filter(cod=40,roll=2)
             relle3  =  Rellenos.objects.filter(cod=40,roll=3)
             relle4  =  Rellenos.objects.filter(cod=40,roll=4)
        
        if kontador == 5:    
             relle1  =  Rellenos.objects.filter(cod=50,roll=1)
             relle2  =  Rellenos.objects.filter(cod=50,roll=2)
             relle3  =  Rellenos.objects.filter(cod=50,roll=3)
             relle4  =  Rellenos.objects.filter(cod=50,roll=4)
             relle5  =  Rellenos.objects.filter(cod=50,roll=5)
        
        if kontador == 6:    
             relle1  =  Rellenos.objects.filter(cod=60,roll=1)
             relle2  =  Rellenos.objects.filter(cod=60,roll=2)
             relle3  =  Rellenos.objects.filter(cod=60,roll=3)
             relle4  =  Rellenos.objects.filter(cod=60,roll=4)
             relle5  =  Rellenos.objects.filter(cod=60,roll=5)
             relle6  =  Rellenos.objects.filter(cod=60,roll=6)
        
        if kontador == 7:    
             relle1  =  Rellenos.objects.filter(cod=70,roll=1)
             relle2  =  Rellenos.objects.filter(cod=70,roll=2)
             relle3  =  Rellenos.objects.filter(cod=70,roll=3)
             relle4  =  Rellenos.objects.filter(cod=70,roll=4)
             relle5  =  Rellenos.objects.filter(cod=70,roll=5)
             relle6  =  Rellenos.objects.filter(cod=70,roll=6)
             relle7  =  Rellenos.objects.filter(cod=70,roll=7)

        #vtot =  valor1_x + valor2_x + valor3_x + valor4_x           

        #para mostrar rellenos disponibles - promo2
        kontador2 = kontador2 - 1
        if kontador2 == 2:    
             relle1_2  =  Rellenos.objects.filter(cod=20,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=20,roll=2)
        
        if kontador2 == 3:    
             relle1_2  =  Rellenos.objects.filter(cod=30,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=30,roll=2)
             relle3_2  =  Rellenos.objects.filter(cod=30,roll=3)
        
        if kontador2 == 4:    
             relle1_2  =  Rellenos.objects.filter(cod=40,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=40,roll=2)
             relle3_2  =  Rellenos.objects.filter(cod=40,roll=3)
             relle4_2  =  Rellenos.objects.filter(cod=40,roll=4)
        
        if kontador2 == 5:    
             relle1_2  =  Rellenos.objects.filter(cod=50,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=50,roll=2)
             relle3_2  =  Rellenos.objects.filter(cod=50,roll=3)
             relle4_2  =  Rellenos.objects.filter(cod=50,roll=4)
             relle5_2  =  Rellenos.objects.filter(cod=50,roll=5)
        
        if kontador2 == 6:    
             relle1_2  =  Rellenos.objects.filter(cod=60,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=60,roll=2)
             relle3_2  =  Rellenos.objects.filter(cod=60,roll=3)
             relle4_2  =  Rellenos.objects.filter(cod=60,roll=4)
             relle5_2  =  Rellenos.objects.filter(cod=60,roll=5)
             relle6_2  =  Rellenos.objects.filter(cod=60,roll=6)
        
        if kontador2 == 7:    
             relle1_2  =  Rellenos.objects.filter(cod=70,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=70,roll=2)
             relle3_2  =  Rellenos.objects.filter(cod=70,roll=3)
             relle4_2  =  Rellenos.objects.filter(cod=70,roll=4)
             relle5_2  =  Rellenos.objects.filter(cod=70,roll=5)
             relle6_2  =  Rellenos.objects.filter(cod=70,roll=6)
             relle7_2  =  Rellenos.objects.filter(cod=70,roll=7)

        vtot =  valor1_x + valor2_x + valor3_x + valor4_x
        vtot3 = vtot

        context = {
                "logo_corp_chico":logo2,
                "descrip_adi1":descrip_adi1,
                "aSubtotal1_adic":aSubtotal1_adic,
                "descrip_adi2":descrip_adi2,
                "aSubtotal2_adic":aSubtotal2_adic,
                "descrip_adi3":descrip_adi3,
                "aSubtotal3_adic":aSubtotal3_adic,
                "caja1":caja1,
                "caja2":caja2,
                "caja3":caja3,
                "caja4":caja4,
                "cod1_x"    : cod1_x     ,
                "descrip1_x": descrip1_x ,
                "valor1_x"  : valor1_x   ,
                "incluye1_x": incluye1_x ,
                "cod2_x"    : cod2_x     ,
                "descrip2_x": descrip2_x ,
                "valor2_x"  : valor2_x   ,
                "incluye2_x": incluye2_x ,
                "cod3_x"    : cod3_x     ,
                "descrip3_x": descrip3_x ,
                "valor3_x"  : valor3_x   ,
                "incluye3_x": incluye3_x ,
                "cod4_x"    : cod4_x     ,
                "descrip4_x": descrip4_x ,
                "valor4_x"  : valor4_x   ,
                "incluye4_x": incluye4_x ,
                "envolt1":envolt1,
                "envolt2":envolt2,
                "envolt3":envolt3,
                "envolt4":envolt4,
                "envolt5":envolt5,
                "envolt6":envolt6,
                "envolt7":envolt7,
                "envolt1_2":envolt1_2,
                "envolt2_2":envolt2_2,
                "envolt3_2":envolt3_2,
                "envolt4_2":envolt4_2,
                "envolt5_2":envolt5_2,
                "envolt6_2":envolt6_2,
                "envolt7_2":envolt7_2,
                "para_envol":para_envol,
                "relle1":relle1,
                "relle2":relle2,
                "relle3":relle3,
                "relle4":relle4,
                "relle5":relle5,
                "relle6":relle6,
                "relle7":relle7,
                "relle1_2":relle1_2,
                "relle2_2":relle2_2,
                "relle3_2":relle3_2,
                "relle4_2":relle4_2,
                "relle5_2":relle5_2,
                "relle6_2":relle6_2,
                "relle7_2":relle7_2,
                "relle_tot":relle_tot,
                "vtot":vtot,
                "vtot3":vtot3,
                "npromo":npromo,
        }
        return render(request,'cambios.html',context)
    return render(request,'principal_flex2.html',context)

def principal_xx(request):
    variable1 = 'PAGINA PRINCIPAL'
    logo2 = "/static/img/Logo_sc.jpg"
    #logo = "/staticfiles/img/Logo_AsistenciaIntegral.jpg" # for PythonAnyWhere
    prs =  Promos.objects.all().order_by('piezas')
    horas =['17:00', '18:00', '19:00', '20:00','21:00'] #provisorio, despues debe hacerce desde tabla
    # Agregando HORAS DISPONIBLES al arreglo        
    string_horas =""
    aDisponibles = [] 
    for j in horas:
        aDisponibles.append(j)
        string_horas = string_horas + j+", "
    string_horas = string_horas[:-2]  # quita los 2 ultimos caracteres (lado derecho quita la coma y el pespacio)
    string_horas = string_horas + " hrs."  

    context ={
        "string_horas":string_horas,
        "prs":prs,
        "variable1":variable1,
        "logo_corp_chico":logo2, }

    npromo = 1    
    kontador = 0      
    kontador2 = 0
    if request.method == "POST":
        #inicializa variales
        cod1_x,descrip1_x,valor1_x,incluye1_x='','',0,''
        cod2_x,descrip2_x,valor2_x,incluye2_x='','',0,''
        cod3_x,descrip3_x,valor3_x,incluye3_x='','',0,''
        cod4_x,descrip4_x,valor4_x,incluye4_x='','',0,''
        envolt1,envolt2,envolt3,envolt4,envolt5,envolt6,envolt7 = "","","","","","",""
        envolt1_2,envolt2_2,envolt3_2,envolt4_2,envolt5_2,envolt6_2,envolt7_2 = "","","","","","",""

        caja1 = request.POST.get('caja1')    # valor desde el template el identificatorio es el <name>
        caja2 = request.POST.get('caja2')    # valor desde el template el identificatorio es el <name>
        caja3 = request.POST.get('caja3')    # valor desde el template el identificatorio es el <name>
        caja4 = request.POST.get('caja4')    # valor desde el template el identificatorio es el <name>
        #
        #
        # para combo seleccionar por que cambiar (todos los tipos de envolturas disponibles)
        para_envol = Envolturas.objects.filter(cod='EN').order_by('roll').only('envolt', 'roll','valor')

        relle1  =  Rellenos.objects.filter(cod=0,roll=0)
        relle2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle3  =  Rellenos.objects.filter(cod=0,roll=0)
        relle4  =  Rellenos.objects.filter(cod=0,roll=0)
        relle5  =  Rellenos.objects.filter(cod=0,roll=0)
        relle6  =  Rellenos.objects.filter(cod=0,roll=0)
        relle7  =  Rellenos.objects.filter(cod=0,roll=0)

        relle1_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle2_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle3_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle4_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle5_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle6_2  =  Rellenos.objects.filter(cod=0,roll=0)
        relle7_2  =  Rellenos.objects.filter(cod=0,roll=0)


        # TODOS LOS RELLENOS DISPONIBLES PARA SELECCIONAR CAMBIAR
        relle_tot = Rellenos.objects.filter(cod="RE")

        #PRMERA PROMO SELECCIONADA
        if  caja1 != "":
            pr1 = Promos.objects.filter(Q(descrip__icontains=caja1))         
            for sal in pr1:
                cod1_x = sal.cod
                descrip1_x = sal.descrip
                valor1_x = sal.valor
                incluye1_x = sal.incluye

            #ENVOLTURAS    
            envolt1 = Envolturas.objects.filter(cod=cod1_x).order_by('roll')

            kontador = 1
            for envolt in envolt1:
                if kontador == 1:
                   envolt1 = envolt.envolt  
                if kontador == 2: 
                   envolt2 = envolt.envolt 
                if kontador == 3: 
                   envolt3 = envolt.envolt 
                if kontador == 4: 
                   envolt4 = envolt.envolt
                if kontador == 5: 
                   envolt5 = envolt.envolt 
                if kontador == 6: 
                   envolt6 = envolt.envolt 
                if kontador == 7: 
                   envolt7 = envolt.envolt 

                kontador = kontador + 1    

        #SEGUNDA PROMO SELECCIONADA SEGUNDA PROMO SELECCIONADA SEGUNDA PROMO SELECCIONADA SEGUNDA PROMO SELECCIONADA 
        if  caja2 != "" or  caja2 == None:
            npromo = npromo + 1
            pr2 = Promos.objects.filter(Q(descrip__icontains=caja2))      
            for sal in pr2:
                cod2_x = sal.cod
                descrip2_x = sal.descrip
                valor2_x = sal.valor
                incluye2_x = sal.incluye

            qr_envolt2  =  Envolturas.objects.filter(cod=cod2_x)
            kontador2 = 1
            for envolt2 in qr_envolt2:
                if kontador2 == 1:
                   envolt1_2 = envolt2.envolt

                if kontador2 == 2: 
                   envolt2_2 = envolt2.envolt

                if kontador2 == 3: 
                   envolt3_2 = envolt2.envolt

                if kontador2 == 4: 
                   envolt4_2 = envolt2.envolt

                if kontador2 == 5: 
                   envolt5_2 = envolt2.envolt

                if kontador2 == 6: 
                   envolt6_2 = envolt2.envolt

                if kontador2 == 7: 
                   envolt7_2 = envolt2.envolt

                kontador2 = kontador2 + 1    

        if caja3 != "":
            pr3 = Promos.objects.filter(Q(descrip__icontains=caja3))         
            nRoll = 1   
            for sal in pr3:
                cod3_x = sal.cod
                descrip3_x = sal.descrip
                valor3_x = sal.valor
                incluye3_x = sal.incluye

            envolt3  =  Envolturas.objects.filter(cod=cod3_x)
            kontador = 1
            for envolt in envolt3:
                if kontador == 1:
                   envolt1_3 = envolt.envolt 

                if kontador == 2: 
                   envolt2_3 = envolt.envolt 

                if kontador == 3: 
                   envolt3_3 = envolt.envolt 

                if kontador == 4: 
                   envolt4_3 = envolt.envolt 

                if kontador == 5: 
                   envolt5_3 = envolt.envolt 

                if kontador == 6: 
                   envolt6_3 = envolt.envolt 

                if kontador == 7: 
                   envolt7_3 = envolt.envolt

                kontador = kontador + 1    

        if caja4 != "":
            pr4 = Promos.objects.filter(Q(descrip__icontains=caja4))         
            nRoll = 1   
            for sal in pr4:
                cod4_x = sal.cod
                descrip4_x = sal.descrip
                valor4_x = sal.valor
                incluye4_x = sal.incluye
            envolt4  =  Envolturas.objects.filter(cod=cod4_x)
            kontador = 1
            for envolt in envolt4:
                if kontador == 1:
                   envolt1 = envolt.envolt  

                if kontador == 2: 
                   envolt2 = envolt.envolt 

                if kontador == 3: 
                   envolt3 = envolt.envolt 

                if kontador == 4: 
                   envolt4 = envolt.envolt 

                if kontador == 5: 
                   envolt5 = envolt.envolt 

                if kontador == 6: 
                   envolt6 = envolt.envolt 

                if kontador == 7: 
                   envolt7 = envolt.envolt 

                kontador = kontador + 1    


        #para mostrar rellenos disponibles - promo1
        kontador = kontador - 1
        if kontador == 2:    
             relle1  =  Rellenos.objects.filter(cod=20,roll=1)
             relle2  =  Rellenos.objects.filter(cod=20,roll=2)
        
        if kontador == 3:    
             relle1  =  Rellenos.objects.filter(cod=30,roll=1)
             relle2  =  Rellenos.objects.filter(cod=30,roll=2)
             relle3  =  Rellenos.objects.filter(cod=30,roll=3)
        
        if kontador == 4:    
             relle1  =  Rellenos.objects.filter(cod=40,roll=1)
             relle2  =  Rellenos.objects.filter(cod=40,roll=2)
             relle3  =  Rellenos.objects.filter(cod=40,roll=3)
             relle4  =  Rellenos.objects.filter(cod=40,roll=4)
        
        if kontador == 5:    
             relle1  =  Rellenos.objects.filter(cod=50,roll=1)
             relle2  =  Rellenos.objects.filter(cod=50,roll=2)
             relle3  =  Rellenos.objects.filter(cod=50,roll=3)
             relle4  =  Rellenos.objects.filter(cod=50,roll=4)
             relle5  =  Rellenos.objects.filter(cod=50,roll=5)
        
        if kontador == 6:    
             relle1  =  Rellenos.objects.filter(cod=60,roll=1)
             relle2  =  Rellenos.objects.filter(cod=60,roll=2)
             relle3  =  Rellenos.objects.filter(cod=60,roll=3)
             relle4  =  Rellenos.objects.filter(cod=60,roll=4)
             relle5  =  Rellenos.objects.filter(cod=60,roll=5)
             relle6  =  Rellenos.objects.filter(cod=60,roll=6)
        
        if kontador == 7:    
             relle1  =  Rellenos.objects.filter(cod=70,roll=1)
             relle2  =  Rellenos.objects.filter(cod=70,roll=2)
             relle3  =  Rellenos.objects.filter(cod=70,roll=3)
             relle4  =  Rellenos.objects.filter(cod=70,roll=4)
             relle5  =  Rellenos.objects.filter(cod=70,roll=5)
             relle6  =  Rellenos.objects.filter(cod=70,roll=6)
             relle7  =  Rellenos.objects.filter(cod=70,roll=7)

        #vtot =  valor1_x + valor2_x + valor3_x + valor4_x           


        #para mostrar rellenos disponibles - promo2
        kontador2 = kontador2 - 1
        if kontador2 == 2:    
             relle1_2  =  Rellenos.objects.filter(cod=20,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=20,roll=2)
        
        if kontador2 == 3:    
             relle1_2  =  Rellenos.objects.filter(cod=30,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=30,roll=2)
             relle3_2  =  Rellenos.objects.filter(cod=30,roll=3)
        
        if kontador2 == 4:    
             relle1_2  =  Rellenos.objects.filter(cod=40,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=40,roll=2)
             relle3_2  =  Rellenos.objects.filter(cod=40,roll=3)
             relle4_2  =  Rellenos.objects.filter(cod=40,roll=4)
        
        if kontador2 == 5:    
             relle1_2  =  Rellenos.objects.filter(cod=50,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=50,roll=2)
             relle3_2  =  Rellenos.objects.filter(cod=50,roll=3)
             relle4_2  =  Rellenos.objects.filter(cod=50,roll=4)
             relle5_2  =  Rellenos.objects.filter(cod=50,roll=5)
        
        if kontador2 == 6:    
             relle1_2  =  Rellenos.objects.filter(cod=60,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=60,roll=2)
             relle3_2  =  Rellenos.objects.filter(cod=60,roll=3)
             relle4_2  =  Rellenos.objects.filter(cod=60,roll=4)
             relle5_2  =  Rellenos.objects.filter(cod=60,roll=5)
             relle6_2  =  Rellenos.objects.filter(cod=60,roll=6)
        
        if kontador2 == 7:    
             relle1_2  =  Rellenos.objects.filter(cod=70,roll=1)
             relle2_2  =  Rellenos.objects.filter(cod=70,roll=2)
             relle3_2  =  Rellenos.objects.filter(cod=70,roll=3)
             relle4_2  =  Rellenos.objects.filter(cod=70,roll=4)
             relle5_2  =  Rellenos.objects.filter(cod=70,roll=5)
             relle6_2  =  Rellenos.objects.filter(cod=70,roll=6)
             relle7_2  =  Rellenos.objects.filter(cod=70,roll=7)

        vtot =  valor1_x + valor2_x + valor3_x + valor4_x

        context = {
                "logo_corp_chico":logo2,
                "caja1":caja1,
                "caja2":caja2,
                "caja3":caja3,
                "caja4":caja4,
                "cod1_x"    : cod1_x     ,
                "descrip1_x": descrip1_x ,
                "valor1_x"  : valor1_x   ,
                "incluye1_x": incluye1_x ,
                "cod2_x"    : cod2_x     ,
                "descrip2_x": descrip2_x ,
                "valor2_x"  : valor2_x   ,
                "incluye2_x": incluye2_x ,
                "cod3_x"    : cod3_x     ,
                "descrip3_x": descrip3_x ,
                "valor3_x"  : valor3_x   ,
                "incluye3_x": incluye3_x ,
                "cod4_x"    : cod4_x     ,
                "descrip4_x": descrip4_x ,
                "valor4_x"  : valor4_x   ,
                "incluye4_x": incluye4_x ,
                "envolt1":envolt1,
                "envolt2":envolt2,
                "envolt3":envolt3,
                "envolt4":envolt4,
                "envolt5":envolt5,
                "envolt6":envolt6,
                "envolt7":envolt7,
                "envolt1_2":envolt1_2,
                "envolt2_2":envolt2_2,
                "envolt3_2":envolt3_2,
                "envolt4_2":envolt4_2,
                "envolt5_2":envolt5_2,
                "envolt6_2":envolt6_2,
                "envolt7_2":envolt7_2,
                "para_envol":para_envol,
                "relle1":relle1,
                "relle2":relle2,
                "relle3":relle3,
                "relle4":relle4,
                "relle5":relle5,
                "relle6":relle6,
                "relle7":relle7,
                "relle1_2":relle1_2,
                "relle2_2":relle2_2,
                "relle3_2":relle3_2,
                "relle4_2":relle4_2,
                "relle5_2":relle5_2,
                "relle6_2":relle6_2,
                "relle7_2":relle7_2,
                "relle_tot":relle_tot,
                "vtot":vtot,
                "npromo":npromo,
        }
        return render(request,'cambios.html',context)
    return render(request,'principal_flex2.html',context)

def galeria(request):
    variable1 = 'Galeria Internacional'
    logo2 = "/static/img/Logo_sc.jpg"
    context ={
    	"variable1":variable1,
    	"logo_corp_chico":logo2, 
    }
    return render(request,'galeria.html',context)


def pedidos_sc(request,pr):
    variable1 = 'Identificación para el pedido'
    variable2 = 'prom'+str(pr)+".jpg"
    logo2 = "/static/img/Logo_sc.jpg"
    context ={
    	"variable1":variable1,
        "variable2":variable2,
    	"logo_corp_chico":logo2,
    	"promo":pr, 
    }
    return render(request,'pedidos.html',context)


def registrarse(request,pr):
    variable1 = 'Pantalla de Registro'
    logo2 = "/static/img/Logo_sc.jpg"
    fecha_x = datetime.now() 
    nombre_x    = request.POST.get('nombre')    # valor del template (id)
    direccion_x = request.POST.get('direccion') # valor del template
    celu_x = request.POST.get('celu')           # valor del template
    if request.method == "POST":
        resultado = 'noexiste'      
        existe = Pedidos.objects.filter(celu=celu_x).exists()
        if existe == True:
            cursor = connection.cursor()
            cursor.execute(
            "update appsclub_pedidos set nombre=%s,direccion=%s,fecha=%s where celu=%s",
            [nombre_x,direccion_x,fecha_x,celu_x])
        else:
            p = Pedidos(nombre=nombre_x,
                direccion=direccion_x,
                celu=celu_x,
                fecha=fecha_x)
            p.save()
    celu_str = str(celu_x)
    celu_str = celu_str[0:1]+" "+celu_str[1:5]+" "+celu_str[5:9] 
    context ={"logo_corp_chico":logo2,
              "nombre":nombre_x,
              "fecha_x":fecha_x,
              "celu_str":celu_str,
              "promo_x":pr,    
              "direccion_x":direccion_x,          
              }
    return render(request,'cambios.html',context) 


def cambios_(request):
    #nombre_x = request.session['nombre_x']        # rescata variable publica  
    nombre_x = "Gabriela Araya Castañeda"
    #direccion_x = request.session['direccion_x']  # rescata variable publica  
    direccion_x = "Pasaje concierto 1879"
    #promo_x = request.session['promo_x']          # rescata variable publica  
    promo_x = "40"
    #celu_x = request.session['celu_x'] 
    celu_x = "977211470"
    context ={"nombre_x":nombre_x,
              "direccion_x":direccion_x,
              "promo_x":promo_x,
              "celu_x":celu_x
              }
    return render(request,'cambios.html',context)

